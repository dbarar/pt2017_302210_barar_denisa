package application;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Logger;

public class ShopQueue implements Runnable{
	
	private final static Logger LOGGER = Logger.getLogger(ShopQueue.class.getName()); 

	private BlockingQueue<Client> queue;
	private AtomicInteger waitingPeriod;//perioada de asteptare la fiecare coada
	private int crt;
	
	//statistics for all the simulation: average waitingTime, serviceTime, emptyTime
	private int nrOfClients = 0;
	private int totalWaitingTime = 0;
	private int totalServiceTime = 0;
	private int emptyTime = 0;
	//statistics for a period of time
	private int nrOfClientsP = 0;
	private int totalWaitingTimeP = 0;
	private int totalServiceTimeP = 0;
	private int emptyTimeP = 0;
	
	public ShopQueue(int crt){
		queue = new LinkedBlockingQueue<Client>();
		this.setCrt(crt);
		setWaitingPeriod(new AtomicInteger(0));
	}
	
	public int getFirstProcessingTime(){
		if(!queue.isEmpty()){
			return queue.peek().getProcessingTime();
		}
		return 0;			
	}
	
	@Override
	public void run() {
		while(ClientGenerator.getCurrentTime() <= ClientGenerator.getTimeLimit()){
			try {
				Thread.sleep(getFirstProcessingTime() * 1000);
				if(!queue.isEmpty()){
					LOGGER.info("clientul cu id-ul " + queue.element().getID() + " a iesit din coada " + crt);
					waitingPeriod.addAndGet(-queue.take().getProcessingTime());	
				}
				else{
					setEmptyTime(getEmptyTime() + 1);
					if(ClientGenerator.getCurrentTime() >= ClientGenerator.getMinStatisticsTime() && ClientGenerator.getCurrentTime() <= ClientGenerator.getMaxStatisticsTime()){
						setEmptyTimeP(getEmptyTimeP() + 1);
					}
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}		
	}
	
	public synchronized BlockingQueue<Client> getQueue(){
		return queue;
	}
	
	public void addClient(Client newClient) throws InterruptedException{
		queue.put(newClient);
		waitingPeriod.addAndGet(newClient.getProcessingTime());
		setTotalWaitingTime(getTotalWaitingTime() + waitingPeriod.intValue());
		setTotalServiceTime(getTotalServiceTime() + newClient.getProcessingTime());
		nrOfClients++;
		
		if(ClientGenerator.getCurrentTime() >= ClientGenerator.getMinStatisticsTime() && ClientGenerator.getCurrentTime() <= ClientGenerator.getMaxStatisticsTime()){
			setTotalWaitingTimeP(getTotalWaitingTimeP() + waitingPeriod.intValue());
			setTotalServiceTimeP(getTotalServiceTimeP() + newClient.getProcessingTime());
			nrOfClientsP++;
		}
	}
	
	public double averageWaitingTime(){
		if(nrOfClients != 0)
			return (double)totalWaitingTime / nrOfClients;
		return 0;
	}
	
	public double averageWaitingTimeP(){
		if(nrOfClientsP != 0)
			return (double)totalWaitingTimeP / nrOfClientsP;
		return 0;
	}
	
	public Client[] getClients(){
		
		return null;
	}

	public int getCrt() {
		return crt;
	}

	public void setCrt(int crt) {
		this.crt = crt;
	}

	public String toString(){
		String s = new String("queue: " + crt + " size: " + queue.size() + "\n");
		for(Client client : queue){
			s = s + client.toString();
		}		
		return s + "\n";
	}

	public AtomicInteger getWaitingPeriod() {
		return waitingPeriod;
	}

	public void setWaitingPeriod(AtomicInteger waitingPeriod) {
		this.waitingPeriod = waitingPeriod;
	}

	public int getTotalServiceTime() {
		return totalServiceTime;
	}

	public void setTotalServiceTime(int totalServiceTime) {
		this.totalServiceTime = totalServiceTime;
	}

	public int getEmptyTime() {
		return emptyTime;
	}

	public void setEmptyTime(int emptyTime) {
		this.emptyTime = emptyTime;
	}

	public int getTotalServiceTimeP() {
		return totalServiceTimeP;
	}

	public void setTotalServiceTimeP(int totalServiceTimeP) {
		this.totalServiceTimeP = totalServiceTimeP;
	}

	public int getEmptyTimeP() {
		return emptyTimeP;
	}

	public void setEmptyTimeP(int emptyTimeP) {
		this.emptyTimeP = emptyTimeP;
	}

	public int getNrOfClientsP() {
		return nrOfClientsP;
	}

	public void setNrOfClientsP(int nrOfClientsP) {
		this.nrOfClientsP = nrOfClientsP;
	}
	
	public int getTotalWaitingTime() {
		return totalWaitingTime;
	}

	public void setTotalWaitingTime(int totalWaitingTimeP) {
		this.totalWaitingTime = totalWaitingTimeP;
	}

	public int getTotalWaitingTimeP() {
		return totalWaitingTimeP;
	}

	public void setTotalWaitingTimeP(int totalWaitingTimeP) {
		this.totalWaitingTimeP = totalWaitingTimeP;
	}
}
