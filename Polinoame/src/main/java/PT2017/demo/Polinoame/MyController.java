package PT2017.demo.Polinoame;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

import javafx.event.ActionEvent;

public class MyController {

    @FXML
    private TextField tf_polinom1;

    @FXML
    private TextField tf_polinom2;

    @FXML
    private TextField tf_result;
    
    @FXML
    private Button addButton;
       
    @FXML
    private Button substractButton;
    
    @FXML
    private Button multiplyButton;
    
    @FXML
    private Button divideButton;
    
    @FXML
    private Button integrateButton;
    
    @FXML
    private Button derivateButton;
    
    @FXML
    private Button calculateButton;
    
    @FXML
    private Model model = new Model();
    
    @FXML
    void addOperation(ActionEvent event){
    	    model.createPolinom1(tf_polinom1.getText());
    	    model.createPolinom2(tf_polinom2.getText());
    	    tf_result.setText((model.addPolinoame(model.getPolinom1(), model.getPolinom2())).toString());
    }
    
    @FXML
    void subOperation(ActionEvent event){
    	    model.createPolinom1(tf_polinom1.getText());
    	    model.createPolinom2(tf_polinom2.getText());
    	    tf_result.setText((model.subPolinoame(model.getPolinom1(), model.getPolinom2())).toString());
    }
    
    @FXML
    void multiplyOperation(ActionEvent event){
	    model.createPolinom1(tf_polinom1.getText());
	    model.createPolinom2(tf_polinom2.getText());
	    Polinom result = model.multiplyPolinoame(model.getPolinom1(), model.getPolinom2());
	    tf_result.setText(result.toString());
    }
    
    @FXML
    void divideOperation(ActionEvent event){
	    model.createPolinom1(tf_polinom1.getText());
	    model.createPolinom2(tf_polinom2.getText());
	    try{
	    	tf_result.setText((model.dividePolinoame(model.getPolinom1(), model.getPolinom2())).toString() + " rest: " + model.getRest().toString());
	    }
	    catch (ArithmeticException e){
	    	tf_result.setText("Divizion by zero!");
	    }
	    catch (Exception e){
	    	tf_result.setText("0");
	    }
    }
    
    @FXML
    void integerDivideOperation(ActionEvent event){
	    model.createPolinom1(tf_polinom1.getText());
	    model.createPolinom2(tf_polinom2.getText());
	    try{
	    	tf_result.setText((model.dividePolinoameIntreg(model.getPolinom1(), model.getPolinom2())).toString() + " rest: " + model.getRest().toString());
	    }
	    catch (ArithmeticException e){
	    	tf_result.setText("Divizion by zero!");
	    }
	    catch (Exception e){
	    	tf_result.setText("ee");
	    }
    }
    
    @FXML
    void integrateOperation(ActionEvent event){
	    model.createPolinom1(tf_polinom1.getText());
	    model.createPolinom2(tf_polinom2.getText());
	    tf_result.setText((model.integratePolinom(model.getPolinom1())).toString());
    }

    @FXML
    void derivateOperation(ActionEvent event){
	    model.createPolinom1(tf_polinom1.getText());
	    model.createPolinom2(tf_polinom2.getText());
	    tf_result.setText((model.derivatePolinom(model.getPolinom1())).toString());
    }
    
    @FXML
    void reset(ActionEvent event){
	    tf_result.setText("");
	    tf_polinom1.setText("");
	    tf_polinom2.setText("");
    }
}